/* eslint-disable no-magic-numbers */
// Task 1
function getAge(birth_date) {
    let today_date = new Date();

    let age = today_date.getFullYear() - birth_date.getFullYear();
    if (today_date.getMonth() < birth_date.getMonth()) {
        age--;
    } else if (today_date.getMonth() === birth_date.getMonth()) {
        if (today_date.getDate() < birth_date.getDate()) {
            age--;
        }
    }

    return age;
}

// Task 2
function getWeekDay(datetime) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    let date = new Date(datetime);
    let day = date.getDay();

    return days[day];
}

// Task 3
function getAmountDaysToNewYear() {
    let today = new Date();
    let newYear = new Date(today.getFullYear() + 1, 0, 1);
    let times = newYear.getTime() - today.getTime();
    let time_day = 24 * 60 * 60 * 100;
    let days = times / time_day;

    return Math.round(days);
}

// Task 4
function getProgrammersDay(year) {
    let date = new Date(year, 0, 1);
    let prog_date = new Date(date.getTime() + 255 * 24 * 60 * 60 * 1000);

    let month = prog_date.toLocaleString('en', {
        month: 'short'
    });
    let week_day = getWeekDay(prog_date);
    let str = prog_date.getDate() + ' ' + month + ', ' + prog_date.getFullYear() + ' (' + week_day + ')';
    return str;
}

// Task 5
function howFarIs(week_day) {
    let today = new Date();
    let days_left = 0;

    for (let time = today.getTime();; time += 1000 * 60 * 60 * 24) {
        let new_date = new Date(time);
        let new_week_dat = getWeekDay(new_date);

        if (new_week_dat === week_day && days_left > 8) {
            break;
        }
        days_left++;
    }

    if (days_left === 0) {
        return `Hey, today is ${ week_day } =)`;
    }
    return `It's ${ days_left } day(s) left till ${ week_day }`;
}

// Task 6
function isValidIdentifier(str) {
    let pattern = /[\D,\W,_,$][\d,\w,a-z,A-Z,$]*/i;
    return str.replace(pattern, '') === '';
}

// Task 7
const testStr = 'My name is John Smith. I am 27.';
capitalize(testStr); // "My Name Is John Smith. I Am 27."

function capitalize(str) {
    str = str.split(/\s+/).map(function (word) {
        return word[0].toUpperCase() + word.substring(1);
    }).join(' ');
    return str;
}

// Task 8
function isValidAudioFile(str) {
    let audio = /^[a-z,A-Z]+\.(flac|mp3|alac|aac)$/;

    if (!audio.test(str)) {
        return false;
    }

    return true;
}

// Task 9
const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';

function getHexadecimalColors(str) {

    let colorRegex = /(#[0-9,a-f,A-F]{3};)|(#[0-9,a-f,A-F]{6};)/gi;
    let rezult = str.match(colorRegex);
    if (rezult === null) {
        return [];
    }
    return rezult;
}

// Task 10
function isValidPassword(password) {
    if (password.length < 8) {
        return false;
    }

    let up_char = /[A-Z]/;
    let small_char = /[a-z]/;
    let dig = /\d/;

    if (!up_char.test(password)) {
        return false;
    }

    if (!small_char.test(password)) {
        return false;
    }

    if (!dig.test(password)) {
        return false;
    }

    return true;
}

// Task 11
function addThousandsSeparators(num) {
    num = num.toString();
    let result = num.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return result;
}

// Task 12
const text1 = 'We use   https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!';

function getAllUrlsFromText(str) {
    if (str === null) {
        return '(error)';
    }

    let urlRegex = /(https?:\/\/[^\s]+)/gi;
    let result = urlRegex.exec(str);
    if (result === null) {
        return [];
    }
    return result;
}
