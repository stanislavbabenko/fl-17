/* START TASK 1: Your code goes here */
/* eslint-disable no-magic-numbers */
const cell = document.querySelectorAll('td');

cell.forEach((element) => element.addEventListener('click', clickCell, false));

function clickCell(event) {
    let td = event.target;
    let tr = td.parentElement;
    let table = tr.parentElement;

    if (td.innerHTML === 'Special Cell') {
        table.style.backgroundColor = 'green'
    } else if (td === tr.querySelector('td:first-child')) {
        tr.style.backgroundColor = 'blue'
    } else {
        td.style.backgroundColor = 'yellow'
    }

}
/* END TASK 1 */

/* START TASK 2: Your code goes here */

const message = document.getElementById('message');
const phone = document.getElementById('phone');
const send = document.getElementById('send');

phone.addEventListener('keyup', function (event) {
    let input = event.target;
    let value = input.value;
    let pattern = /\+380\d{9}/;

    if (value.replace(pattern, '').length === 0) {
        message.style.display = 'none';
        send.removeAttribute('disabled');
        phone.style.borderColor = 'grey';
    } else {
        message.innerHTML = 'Type number does not follow format +380*********';
        message.style.backgroundColor = '#f88';
        message.style.display = 'block';
        send.setAttribute('disabled', true);
        phone.style.borderColor = 'red';
    }
})
send.addEventListener('click', function () {
    message.innerHTML = 'Data was successfully send';
    message.style.backgroundColor = 'green';
    message.style.display = 'block';
});

/* END TASK 2 */

/* START TASK 3: Your code goes here */
const court = document.querySelector('#task3 #court');
const ball = document.querySelector('#task3 #ball');
const scorebar = document.querySelector('#task3 #scorebar');
let score = {
    a: 0,
    b: 0
};
const event_team_a = new CustomEvent('event_add_score', {
    'detail': 'a'
});
const event_team_b = new CustomEvent('event_add_score', {
    'detail': 'b'
});

court.addEventListener('click', function (event) {
    let court_block = event.target.getBoundingClientRect();
    let x = event.clientX - court_block.left;
    let y = event.clientY - court_block.top;

    ball.style.left = x - 20 + 'px';
    ball.style.top = y - 20 + 'px';

    if (y >= court_block.height / 2 - 15 && y <= court_block.height / 2 + 15) {
        if (x - 30 <= 30 + 15) {
            scorebar.dispatchEvent(event_team_a);
        }
        if (x + 30 >= court_block.width - 15) {
            scorebar.dispatchEvent(event_team_b);
        }
    }
});

scorebar.addEventListener('event_add_score', function (event) {
    let team = event.detail;
    score[team] += 1;
    scorebar.querySelector('.team_' + team + ' span').innerHTML = score[team];
    scorebar.querySelector('.mess.team_' + team).style.display = 'block';
    setTimeout(function () {
        scorebar.querySelector('.mess.team_' + team).style.display = 'none';
    }, 3000);
});
/* END TASK 3 */
