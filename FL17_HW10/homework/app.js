/* eslint-disable no-magic-numbers */
const root = document.getElementById('root');

const html_add_item = `<div id="alertMessage" class="hidden"><span id="alertMessageText"></span></div>
        <div id="addItem" class="add">
            <h1 id="addItemHeader">Add tweet</h1>
            <textarea maxlength="140" id="addItemInput"></textarea>
            <div class="addButtons">
                <button id="cancelAdd">Cancel</button>
                <button id="saveAddItem">Save Changes</button>
            </div>
        </div>`;
const html_nav_liked = `
                <button class="addTweet">Add tweet</button>
                <button class="goLikeTweet">Go to liked</button>`;
const html_nav_liked_page = `
                <button class="back">back</button>
                <button class="addTweet">Add tweet</button>`;

const html_edit_tweet = `<div id="alertMessage" class="hidden"><span id="alertMessageText"></span></div>
        <div id="modifyItem" class="hidden">
			<h1 id="modifyItemHeader">Edit tweet</h1>
			<textarea maxlength="140" id="modifyItemInput"></textarea>
			<div class="formButtons">
				<button id="cancelModification">Cancel</button>
				<button id="saveModifiedItem">Save Changes</button>
			</div>
		</div>`;


let tweets = window.localStorage.getItem('tweets');
if (tweets === null) {
    tweets = [];
} else {
    tweets = JSON.parse(tweets);
}
console.log(tweets);

function add_twit(text) {
    let new_twit = {
        text: text,
        like: false
    };
    tweets.push(new_twit);
    window.localStorage.setItem('tweets', JSON.stringify(tweets));
}

function edit_twit(index, text) {
    tweets[index]['text'] = text;
    window.localStorage.setItem('tweets', JSON.stringify(tweets));
}

function removeTweet(index) {
    delete tweets[index];
    window.localStorage.setItem('tweets', JSON.stringify(tweets));
}

function likeTweet(index) {
    tweets[index]['like'] = !tweets[index]['like'];
    window.localStorage.setItem('tweets', JSON.stringify(tweets));
    return tweets[index]['like']
}

let target = window.location.hash;
if (target) {
    if (target === '#/add') {
        view_add_item();
    } else if (target === '#/liked') {
        view_list(true);
    } else if (target.indexOf('edit') !== -1) {
        let index = target.split(':')[1] * 1;
        view_edit_item(index);
    }
} else {
    view_list(false);
}

function href(link) {
    window.location.replace(link);
    document.location.href = link;
    location.reload();
}

function view_add_item() {
    document.getElementById('tweetItems').style.display = 'none';
    root.innerHTML = html_add_item;

    document.querySelector('#cancelAdd').addEventListener('click', function () {
        window.location.replace(window.location.href.split('#')[0]);
        document.location.href = window.location.href.split('#')[0];
    });

    document.querySelector('#saveAddItem').addEventListener('click', function () {
        let text = document.querySelector('#addItemInput').value;
        if (!check_tweet(text)) {
            add_twit(text);

            window.location.replace(window.location.href.split('#')[0]);
            document.location.href = window.location.href.split('#')[0];
        } else {
            view_message('You can not tweet this text!');
        }
    });
}

function view_edit_item(index) {
    document.getElementById('modifyItem').style.display = 'block';
    root.innerHTML = html_edit_tweet;
    document.getElementById('modifyItemInput').value = tweets[index]['text'];

    document.querySelector('#cancelModification').addEventListener('click', function () {
        window.location.replace(window.location.href.split('#')[0]);
        document.location.href = window.location.href.split('#')[0];
    });

    document.querySelector('#saveModifiedItem').addEventListener('click', function () {
        let text = document.querySelector('#modifyItemInput').value;
        if (!check_tweet(text)) {
            edit_twit(index, text);

            window.location.replace(window.location.href.split('#')[0]);
            document.location.href = window.location.href.split('#')[0];
        } else {
            view_message('You can not tweet this text!');
        }
    });
}

function view_list(islike) {
    document.getElementById('modifyItem').style.display = 'none';
    tweets.forEach(function (elem, index) {
        if (elem !== null) {
            let new_li = document.createElement('li');
            let like_text = 'like';
            if (elem['like'] === true) {
                like_text = 'unlike';
            }
            new_li.innerHTML += `<span data-index="${index}">${elem.text}</span>`;
            new_li.innerHTML += `<input type="button" data-index="${index}" class="removeTweet" value="remove">`;
            new_li.innerHTML += `<input type="button" value="${like_text}" data-index="${index}" class="likeTweet">`;

            if (islike === false) {
                document.getElementById('list').append(new_li);
            } else {
                if (elem['like'] === true) {
                    document.getElementById('list').append(new_li);
                    document.querySelector('#tweetItems h1').innerHTML = 'Liked Tweets';
                }
            }
        }
    });


    let remove_tweets = document.querySelectorAll('.removeTweet');
    remove_tweets.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            let button = event.target;
            removeTweet(button.getAttribute('data-index'));
            button.parentElement.remove();
        });
    });

    let like_tweets = document.querySelectorAll('.likeTweet');
    like_tweets.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            let button = event.target;
            let islike = likeTweet(button.getAttribute('data-index'));
            if (islike === true) {
                view_message('You like tweet with id ' + button.getAttribute('data-index'));
                button.value = 'unlike';

            } else {
                view_message('You unlike tweet with id ' + button.getAttribute('data-index'));
                button.value = 'like';
            }
        });
    });

    if (islike === false) {
        let liked_tweets = document.querySelectorAll('.likeTweet[value="unlike"]');
        if (liked_tweets.length > 0) {
            document.getElementById('navigationButtons').innerHTML = html_nav_liked;

            document.querySelector('.goLikeTweet').addEventListener('click', function () {
                href('#/liked');
            });
        }
    } else {
        document.getElementById('navigationButtons').innerHTML = html_nav_liked_page;
        document.querySelector('.back').addEventListener('click', function () {
            window.location.replace(window.location.href.split('#')[0]);
            document.location.href = window.location.href.split('#')[0];
        });
    }

    let text_tweets = document.querySelectorAll('li span');
    text_tweets.forEach(function (elem) {
        elem.addEventListener('click', function (event) {
            let button = event.target;
            href('#/edit/:' + button.getAttribute('data-index'));
        });
    });


    document.querySelector('.addTweet').addEventListener('click', function () {
        href('#/add');
    });
}

function view_message(text) {
    let mess_block = document.getElementById('alertMessageText');
    mess_block.innerHTML = text;
    setTimeout(function () {
        mess_block.innerHTML = '';
    }, 3000);
}

function check_tweet(text) {
    let search = tweets.filter(function (elem) {
        return elem !== null && elem['text'] === text
    });
    if (search.length > 0) {
        return true;
    }
    return false;
}
