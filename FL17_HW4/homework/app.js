let data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39,
        'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
];

function reverseNumber(num) {
    num = num.toString();
    let newNum = '';

    for (let i = num.length - 1; i > 0; i--) {
        newNum = newNum + num[i];
    }

    if (num[0] === '-'){
        newNum = '-' + newNum;
    } else {
        newNum = newNum + num[0];
    }

    return newNum;

}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }

}

function map(arr, func) {
    let newArr = [];

    forEach(arr, function (el) {
        newArr.push(func(el));
    });
    return newArr;
}

function filter(arr, func) {
    let newArr = [];

    forEach(arr, function (el) {
        if (func(el)) {
            newArr.push(el);
        }
    });

    return newArr;

}

function getAdultAppleLovers(data) {
    let newArr = [];

    newArr = filter(data, function (el) {
        // eslint-disable-next-line no-magic-numbers
        return el.age > 18 && el.favoriteFruit === 'apple';
    });

    newArr = map(newArr, function (el) {
        return el.name;
    });

    return newArr;
}

function getKeys(obj) {
    let keys = [];
    /*eslint guard-for-in: "error"*/
    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            keys.push(key);
        }
    }
    return keys;
}

function getValues(obj) {
    let Values = [];
    /*eslint guard-for-in: "error"*/
    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            Values.push(obj[key]);
        }
    }
    return Values;
}

function showFormattedDate(dateObj) {
    let day = dateObj.getDate();
    let month = dateObj.toLocaleString('en', {
        month: 'short'
    });
    let year = dateObj.getFullYear();

    return 'It is ' + day + ' of ' + month + ', ' + year;
}
