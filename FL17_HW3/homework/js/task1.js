/* eslint-disable no-magic-numbers */
let init_amount = get_amount('Введите начальную сумму:', 1000);
let years = get_years('Введите количество лет:', 1);
let percentage = get_percentage('Введите %/год:', 100);

let total_amount = init_amount * 1;

for (let i = 0; i < years; i++) {
    total_amount += total_amount / 100 * percentage
}

// eslint-disable-next-line max-len
alert('Initial amount: ' + init_amount + '\nNumber of years: ' + years + '\nPercentage of year: ' + percentage + '\n\nTotal profit: ' + (total_amount - init_amount).toFixed(2) + '\nTotal amount: ' + total_amount.toFixed(2));

// function for get only number
function get_amount(title, min) {
    let numb = prompt(title);
    if (numb.replace(/\s/g, '').length === 0 || isNaN(numb)) {
        alert('Invalid input data!');
        numb = get_amount(title, min);
    }
    if (numb < min) {
        alert('Invalid input data!');
        numb = get_amount(title, min);
    }
    return numb * 1;
}
// function for get only number
function get_percentage(title, max) {
    let numb = prompt(title, []);
    if (numb.replace(/\s/g, '').length === 0 || isNaN(numb)) {
        alert('Invalid input data!');
        numb = get_percentage(title, max);
    }
    if (numb > max) {
        alert('Invalid input data!');
        numb = get_percentage(title, max);
    }
    return numb * 1;
}

// function for get only integer
function get_years(title, min) {
    let numb = prompt(title, []);
    if (!Number.isInteger(numb * 1)) {
        alert('Invalid input data!');
        numb = get_years(title, min);
    }
    if (numb < min) {
        alert('Invalid input data!');
        numb = get_years(title, min);
    }
    return numb * 1;
}