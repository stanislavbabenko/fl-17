/* eslint-disable no-magic-numbers */
// Your code goes here

let game_n = 1;
let total_prize = 0;

let want_play = confirm('Do you want to play a game?');
// eslint-disable-next-line no-constant-condition
while (true) {
	if (want_play) {
		total_prize += play_random(game_n, total_prize);
		game_n++;
		
		if (total_prize > 0) {
			want_play = confirm(`Congratulation, you won Your prize is: ${total_prize} $. Do you want to continue?`)
		} else {
			// eslint-disable-next-line max-len
			want_play = confirm(`Thank you for your participation. Your prize is ${total_prize} $. Do you want to continue?`);
		}
	} else {
		alert('You did not become a billionaire, but can.');
		break;
	}
}

// function for play
function play_random(game_n, total_prize) {
	let min = 0;
	// eslint-disable-next-line no-magic-numbers
	let max = 8 + (game_n - 1) * 4;
	let random_numb = getRandomIntInclusive(min, max);
	//random_numb = 5;

	// eslint-disable-next-line no-magic-numbers
	let prize = 100 * game_n;
	for (let i = 1; i <= 3; i++) {
		// eslint-disable-next-line max-len
		let message = 'Choose a roulette pocket from ' + min + ' to ' + max + '\nAttempts left ' + (3 - i + 1) + '\nTotal prize ' + total_prize + '\nPossible prize on current attempt: ' + prize + '\n';

		let numb_pocket = get_integer_diapason(message, min, max);
		// eslint-disable-next-line eqeqeq
		if (random_numb == numb_pocket) {
			return prize;
		} else {
			if (i < 3) {
				alert('You wrong. Lets again');
				prize = prize / 2;
			} else {
				prize = 0;
			}
		}
	}
	return prize;
}

// function for get integer random number
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// function for get only number
function get_integer_diapason(title, min, max) {
    let numb = prompt(title, []);
    if (!Number.isInteger(numb * 1)) {
        alert('Can be only integers!');
        numb = get_integer_diapason(title, max, min);
    }
    if (numb * 0 < min || numb * 0 > max) {
        alert(`Can be from ${min} to ${max}!`);
        numb = get_integer_diapason(title, max, min);
    }
    return numb * 1;
}
