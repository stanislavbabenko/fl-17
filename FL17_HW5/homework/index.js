// Your code goes here
function isEquals(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames(item1, item2, item3) {
    let arr = [item1, item2, item3];
    return arr;

}

function getDifference(a, b) {
    if (a<b){
        return b-a;
    } else{
        return a - b;
    }    
}

function negativeCount(arr) {
    let n = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            n = n + 1;
        }
    }
    return n;
}


function letterCount(str, char) {
    let n = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === char) {
            n = n + 1;
        }
    }
    return n;
}

function countPoints(arr) {
    let point = 0;
    for (let i = 0; i < arr.length; i++) {
        let nums = arr[i].split(':');
        let new_point = 3;
        if (nums[0] * 1 > nums[1] * 1) {
            point += new_point;
        } else if (nums[0] * 1 < nums[1] * 1) {
            point += 0;
        } else {
            point += 1;
        }
    }
    return point;
}
