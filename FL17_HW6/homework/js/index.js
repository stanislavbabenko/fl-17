function visitLink(path) {
    let visit = localStorage.getItem(path) * 1;
    localStorage.setItem(path, visit + 1);
}


function viewResults() {
    let ul = document.createElement('ul');
    
    let arr_path = ['Page1', 'Page2', 'Page3'];
	for (let i = 0; i < arr_path.length; i++) {
        let visit = localStorage.getItem(arr_path[i]) * 1;
        let li = document.createElement('li');
        li.innerHTML = 'You visited ' + arr_path[i] + ' ' + visit + ' time(s)';
        ul.append(li);
    }

    document.getElementById('content').append(ul);
    localStorage.clear();
}
