/* eslint-disable no-magic-numbers */
const appRoot = document.getElementById('app-root');

/* Base html */
const html_base = `
<h1>Countries Search</h1>
<div class="block">
    <p>Please choose type of search: </p>
    <div class="block_radio">
        <label><input name="type" type="radio" value="region"> By Region</label>
        <label><input name="type" type="radio" value="language"> By Language</label>
    </div>
</div>
<div class="block">
    <label for="query">Please choose search query: </label>
    <select disabled id="query">
        <option select value="">Select value</option>
    </select>
</div>
<div id="rezult">
    <table>
        <thead>
            <tr>
                <th>Country Name <span id="sort_by_name">↓</span></th>
                <th>Capital</th>
                <th>World Region</th>
                <th>Languages</th>
                <th>Area <span id="sort_by_area">↕</span></th>
                <th>Flag</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <p>No items, please choose search query</p>
</div>`;

/* Render base html */
appRoot.innerHTML = html_base;

/* Global variable */
let type = 'region';
let rezult = [];
/* Const with html items */
const inputType = document.querySelectorAll('input[name="type"]');
const selectorQuery = document.getElementById('query');

const buttonSortName = document.getElementById('sort_by_name');
const buttonSortArea = document.getElementById('sort_by_area');

const rezultTable = document.querySelector('#rezult table');
const paragraf = document.querySelector('#rezult p');

/* Add events for html items */
inputType.forEach(function (elem) {
    elem.addEventListener('change', changeInputType);
});
selectorQuery.addEventListener('change', changeSelectorQuery);
buttonSortName.addEventListener('click', clickSortName);
buttonSortArea.addEventListener('click', clickSortArea);

/* Functions for events */
function changeInputType(event) {
    type = event.target.value;
    let list = [];
    if (type === 'region') {
        list = externalService.getRegionsList();
    } else {
        list = externalService.getLanguagesList();
    }

    rezultTable.style.display = 'none';
    paragraf.style.display = 'block';

    selectorQuery.innerHTML = '<option select value="">Select value</option>';
    list.forEach(function (elem) {

        let option = document.createElement('option');
        option.innerHTML = elem;

        selectorQuery.append(option);
    })
    selectorQuery.removeAttribute('disabled');
}

function changeSelectorQuery(event) {
    let query = event.target.value;

    if (type === 'region') {
        rezult = externalService.getCountryListByRegion(query);
    } else {
        rezult = externalService.getCountryListByLanguage(query);
    }
    if (rezult.length === 0) {
        rezultTable.style.display = 'none';
        paragraf.style.display = 'block';
    } else {
        sort_data('name', 'asc');
        renderTable();
        paragraf.style.display = 'none';
        rezultTable.style.display = 'table';
    }
}

function clickSortName(event) {
    let sort_arrow_item = event.target;

    if (sort_arrow_item.innerHTML === '↓') {
        sort_arrow_item.innerHTML = '↑';
        sort_data('name', 'desc');
    } else {
        sort_arrow_item.innerHTML = '↓';
        sort_data('name', 'asc');
    }
    renderTable();
}

function clickSortArea(event) {
    let sort_arrow_item = event.target;

    if (sort_arrow_item.innerHTML === '↓') {
        sort_arrow_item.innerHTML = '↑';
        sort_data('area', 'desc');
    } else {
        sort_arrow_item.innerHTML = '↓';
        sort_data('area', 'asc');
    }
    renderTable();
}

function renderTable() {

    rezultTable.querySelector('tbody').innerHTML = '';

    rezult.forEach(function (elem) {

        let tr = document.createElement('tr');

        let tdName = document.createElement('td');
        tdName.innerHTML = elem['name'];
        tr.append(tdName);

        let tdCapital = document.createElement('td');
        tdCapital.innerHTML = elem['capital'];
        tr.append(tdCapital);

        let tdRegion = document.createElement('td');
        tdRegion.innerHTML = elem['region'];
        tr.append(tdRegion);

        let tdLanguage = document.createElement('td');
        tdLanguage.innerHTML = Object.values(elem['languages']).join(', ');
        tr.append(tdLanguage);

        let tdArea = document.createElement('td');
        tdArea.innerHTML = elem['area'];
        tr.append(tdArea);

        let tdFlag = document.createElement('td');
        tdFlag.innerHTML = `<img src="${elem['flagURL']}">`;
        tr.append(tdFlag);

        rezultTable.querySelector('tbody').append(tr);

    });
}

function sort_data(sort_by, sort_type) {
    rezult = rezult.sort(function (a, b) {
        if (sort_type === 'asc') {
            if (a[sort_by] < b[sort_by]) {
                return -1;
            }
            if (a[sort_by] > b[sort_by]) {
                return 1;
            }
            return 0;
        } else {
            if (a[sort_by] > b[sort_by]) {
                return -1;
            }
            if (a[sort_by] < b[sort_by]) {
                return 1;
            }
            return 0;
        }
    });
}